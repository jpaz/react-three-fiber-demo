import { OrbitControls, softShadows } from "drei";
import React from "react";
import { Canvas } from "react-three-fiber";
import SpinningMesh from "./components/SpinningMesh";

softShadows({
	frustrum: 3.75, // Frustrum width (default: 3.75)
	size: 0.005, // World size (default: 0.005)
	near: 9.5, // Near plane (default: 9.5)
	samples: 17, // Samples (default: 17)
	rings: 11, // Rings (default: 11)
});

function App() {
	return (
		<Canvas style={{ width: "100vw", height: "100vh" }} colorManagement shadowMap camera={{ position: [-5, 2, 10], fov: 60 }}>
			<ambientLight intensity={0.3} />
			<directionalLight
				castShadow
				position={[0, 10, 0]}
				intensity={1.5}
				shadow-mapSize-width={1024}
				shadow-mapSize-height={1024}
				shadow-camera-far={50}
				shadow-camera-left={-10}
				shadow-camera-right={10}
				shadow-camera-top={10}
				shadow-camera-bottom={-10}
			/>
			<pointLight position={[-10, 0, -20]} intensity={0.5} />
			<pointLight position={[0, -10, 0]} intensity={1.5} />
			<group>
				{/* Floor */}
				<mesh rotation={[-Math.PI / 2, 0, 0]} position={[0, -3, 0]} receiveShadow>
					<planeBufferGeometry attach="geometry" args={[100, 100]} />
					<shadowMaterial attach="material" opacity={0.3} />
				</mesh>
				<SpinningMesh position={[0, 1, 0]} color="lightblue" args={[3, 2, 1, 2, 1, 20]} speed={2} />
				<SpinningMesh position={[0, 1, 4]} color="red" args={[3, 1, 1]} speed={2} />
				<SpinningMesh position={[-2, 1, -5]} color="pink" speed={6} />
				<SpinningMesh position={[5, 1, -2]} color="yellow" speed={6} />
			</group>
			<OrbitControls />
		</Canvas>
	);
}

export default App;
