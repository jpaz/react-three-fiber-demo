import { a, useSpring } from "@react-spring/three";
import { MeshWobbleMaterial } from "drei";
import React, { useRef, useState } from "react";
import { useFrame, Vector3 } from "react-three-fiber";
import { Mesh } from "three";

export interface SpinningMeshProps {
	position?: Vector3;
	color?: string | number;
	speed?: number;
	args?: any;
}

const SpinningMesh = ({ position, color, speed, args }: SpinningMeshProps) => {
	const mesh = useRef<Mesh>();
	const [expand, setExpand] = useState(false);
	const props = useSpring({
		scale: expand ? [1.4, 1.4, 1.4] : [1, 1, 1],
	});

	useFrame(() => {
		if (mesh.current) {
			mesh.current.rotation.x = mesh.current.rotation.y += 0.01;
		}
	});

	return (
		<a.mesh position={position} ref={mesh} onClick={() => setExpand((e) => !e)} scale={(props.scale as unknown) as Vector3} castShadow>
			<boxBufferGeometry attach="geometry" args={args} />
			<MeshWobbleMaterial color={color} speed={speed} attach="material" factor={0.5} />
		</a.mesh>
	);
};

export default SpinningMesh;
